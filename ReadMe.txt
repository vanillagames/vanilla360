Notes so far:

It seems the one shader [360/SkyboxPanoramicBeta] can display both photos and video just the same. Awesome! Lets call this the 360 shader.
The main difference is the video player requires a render texture with potentially different settings like resolution and color format, whereas for photos, the material can simply contain the image itself.

The setup for photos seems to go as such:

MeshRenderer/Skybox using the 360Photo material which contains a 360 texture/image.

Photos on a sphere:
	- Create a generic sphere
	- Attach your Panoramic Photo Controller
	- Populate the 'Textures' array with any number of 360 pictures
	- Assign the 360Photo material to the sphere. The material settings may need a bit of tweaking if it isn't displaying correctly.
	- That's it, chuck a camera in and it should work. You can cycle through the available pictures with the CycleTextures(true=up/false=down) function.
	
Photos on a skybox:
	- Create a new scene with an empty game object
	- Attach and setup your Panoramic Photo Controller script as above.
	- Make sure this is the currently 'Active' scene by right-clicking the scene name and selecting "Set Active Scene"
	- Now open the Lighting settings for the scene and set the default skybox material to be the 360Photo material. The lighting window may need to be closed and opened if it was already open at the beginning of this step.
	- Make sure the scene your cameras are in have the Skybox material in the Lighting settings set to none, otherwise they may render on top.
	- That's it! Cycle as above.
	
The setup for video seems to go:

MeshRenderer/Skybox using a Unity 'Video Player' component which draws onto a RenderTexture which is -then- used by the 360Video material.
	
Video on a sphere:
	- Create a generic sphere
	- Attach the 360Video material to the sphere. The settings shouldn't be any different from the 360Photo material.
	- Attach one of Unity's Video Player components to the sphere and set it to automatically play and loop.
	- Give it a compatible 360 video clip (You haven't programmed any clip array at the time of writing!)
	- Attach your Panoramic Video Controller component
	- Assign however many RenderTextures you need to the 'Resolutions' array. A RenderTextures dimensions can't be changed in code, so you might need one for each kind of resolution and also each kind of Color Format! Some are device-specific!

Video on a skybox:
	- Empty gameobject
	- Panoramic Video Controller component with RenderTextures applied
	- Video Player component with 360Video material and compatible video clip set to automatically play and loop
	- Make sure the Lighting settings for this scene has its skybox set to the 360Video material.
	- Make sure the camera scenes skybox is set to empty.